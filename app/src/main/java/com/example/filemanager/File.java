package com.example.filemanager;

import android.net.Uri;

public class File {
    private final Uri uri;
    private final String name;
    private final String size;

    public File(Uri uri, String name, String size) {
        this.uri = uri;
        this.name = name;
        this.size = size;
    }

    public String getSize() {
        return size;
    }

    public Uri getUri() {
        return uri;
    }

    public String getName() {
        return name;
    }
}
