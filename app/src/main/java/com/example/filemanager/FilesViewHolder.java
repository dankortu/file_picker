package com.example.filemanager;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.filemanager.databinding.FileBinding;



public class FilesViewHolder extends RecyclerView.ViewHolder {
    private final FileBinding binding;

    public FilesViewHolder(@NonNull FileBinding fileView) {
        super(fileView.getRoot());
        binding=fileView;
    }

    public void bind(File file) {
        binding.fileIcon.setImageURI(file.getUri());
        binding.fileName.setText(file.getName());
        binding.fileSize.setText(file.getSize());
    }
}
