package com.example.filemanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.filemanager.databinding.FileBinding;


import java.util.ArrayList;
import java.util.List;

public class FilesAdapter extends RecyclerView.Adapter<FilesViewHolder> {
    private final List<File> files = new ArrayList<>();


    @NonNull
    @Override
    public FilesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        return new FilesViewHolder(FileBinding.inflate(inflater, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FilesViewHolder holder, int position) {
        File file = files.get(position);
        holder.bind(file);
    }

    public  void setFiles(List<File> files) {
        this.files.clear();
        this.files.addAll(files);

        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return files.size();
    }
}
