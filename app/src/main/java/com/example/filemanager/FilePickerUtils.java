package com.example.filemanager;

import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.util.Log;

import java.util.List;

public class FilePickerUtils {

    public static void pickFiles(Activity activity) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.addCategory(Intent.CATEGORY_OPENABLE);


        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        sIntent.putExtra("CONTENT_TYPE", "*/*");
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (activity.getPackageManager().resolveActivity(sIntent, 0) != null){
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        } else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }


        activity.startActivityForResult(chooserIntent, 420);
    }

    public static void getFileData (Intent data, Activity activity, List<File> files) {
        ClipData cd = data.getClipData();

        if (cd == null) {
            Uri itemUri = data.getData();

            files.add(FilePickerUtils.getItemProperties(itemUri, activity));

        } else {
            for (int i=0; i < cd.getItemCount(); i++) {
                ClipData.Item item = cd.getItemAt(i);
                Uri itemUri = item.getUri();

                files.add(FilePickerUtils.getItemProperties(itemUri, activity));
            }
        }
    }

    private static File getItemProperties (Uri itemUri, Activity activity) {

        String expansion = activity.getContentResolver().getType(itemUri);
        Log.i("EXPANSION", expansion);
        Cursor returnCursor = activity.getContentResolver().query(itemUri, null, null, null, null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);

        returnCursor.moveToFirst();
        String itemName = returnCursor.getString(nameIndex);
        String itemSize = Long.toString(returnCursor.getLong(sizeIndex));
        returnCursor.close();

        return new File(itemUri, itemName, itemSize);
    }
}
